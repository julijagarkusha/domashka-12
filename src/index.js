import { TaskList } from './modules/TaskList.js';
import { updateHTML } from './modules/htmlController.js';
import { getFilterButtonLabel } from './modules/utils.js';

const tasksFormElement = document.querySelector('.tasks__form');
const tasksFormInput = document.getElementById('taskName');
const tasksFormErrorElement = tasksFormElement.querySelector('.input__error');
const tasksFilterButtons = document.querySelectorAll('.tasks__filter .btn');

let taskListKey = 'all';

const taskList = new TaskList(() => {
  updateHTML(taskList[taskListKey], taskList);
});

updateHTML(taskList[taskListKey], taskList);

tasksFilterButtons.forEach(tasksFilterButton => {
  tasksFilterButton.innerText = getFilterButtonLabel(tasksFilterButton, taskList);
});

tasksFormElement.addEventListener('submit', (event) => {
  event.preventDefault();
  let formData = new FormData(event.target);
  const taskName = formData.get('taskName').trim();

  if (!taskName.length) {
    tasksFormErrorElement.innerText = 'Required field!';
    tasksFormErrorElement.classList.add('input__error--show');
    return;
  }

  taskList.createTask(taskName);
  formData.set('taskName', '');

  event.target.querySelector('.tasks__input').value = '';
});

tasksFormInput.addEventListener('input', () => {
  tasksFormErrorElement.classList.remove('input__error--show');
});

const filterClickHandler = (event) => {
  const activeFilterClassName = 'btn-outline--active';
  tasksFilterButtons.forEach(tasksFilterButton => {
    tasksFilterButton.classList.remove(activeFilterClassName);
  });
  event.target.classList.add(activeFilterClassName);

  taskListKey = event.target.getAttribute('data-filter');
  updateHTML(taskList[taskListKey], taskList);
};

tasksFilterButtons.forEach(tasksFilterButton => {
  tasksFilterButton.addEventListener('click', filterClickHandler);
});
