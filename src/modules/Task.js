export function Task(name, onUpdateHandler, isDone= false, id = Date.now()) {
  this.id = id;
  this.name = name;
  this.isDone = isDone;

  this.setCompleted = function () {
    this.isDone = true;
    onUpdateHandler();
  };

  this.setInProgress = function () {
    this.isDone = false;
    onUpdateHandler();
  };

  this.edit = function (name) {
    this.name = name;
    onUpdateHandler();
  }
}
