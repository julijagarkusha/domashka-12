import { Task } from './Task.js';
import {
  setDataStorage,
  getDataStorage,
} from './storage.js';

export function TaskList(onUpdateHandler = () => {
}) {
  this.onUpdateData = function () {
    setDataStorage('taskList', this.tasksList.map(task => ({
      name: task.name,
      isDone: task.isDone,
      id: task.id,
    })));

    onUpdateHandler();
  }.bind(this);

  this.tasksList = getDataStorage('taskList', [])
    .map(taskData => new Task(taskData.name, this.onUpdateData, taskData.isDone, taskData.id));

  this.createTask = function (name) {
    this.tasksList.push(new Task(name, this.onUpdateData));
    this.onUpdateData();
  };

  this.removeTask = function (id) {
    this.tasksList = this.tasksList.filter(item => item.id !== id);
    this.onUpdateData();
  };

  Object.defineProperties(this, {
    inProgress: { get: () => this.tasksList.filter(item => !item.isDone) },
    completed: { get: () => this.tasksList.filter(item => item.isDone) },
    all: { get: () => this.tasksList },
  });
}
