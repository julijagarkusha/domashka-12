export const setDataStorage = (key, data) => {
  localStorage.setItem(key, JSON.stringify(data));

  return data;
}

export const getDataStorage = (key, defaultData) => {
  const localStorageData = localStorage.getItem(key);

  if(!localStorageData) {
    return setDataStorage(key, defaultData);
  }

  return JSON.parse(localStorageData);
}
